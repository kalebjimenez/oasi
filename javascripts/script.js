/* VALIDATIONS */

$( document ).ready(function() {

  //Validations

  $("#form-contact").validate({
   messages: {
     name: "Ingresa al menos un nombre y apellido",
     telephone: "Es importante para contactarte ingresar un número"
   }
});
  $("#form-guarantee").validate();
  $("#form-sales-team").validate({
    messages: {
      name: "Ingresa al menos un nombre y apellido",
      telephone: "Es importante para contactarte ingresar un número"
    }
  });



  //Testimonials

  $('.testimonials').slick({
    autoplay: true,
    dots: false,
    pauseOnDotsHover: true,
    customPaging : function() {
      return '<a class="slick-dot"></a>';
    },
    autoplaySpeed: 8000,
    prevArrow: $('.prev-testimonial'),
    nextArrow: $('.next-testimonial')
  });



  //Faq questions

  $(".faq__answer").hide();

  $(".faq__question").on('click',function(){
    var answers = $(this).closest(".faq__item").children(".faq__answer");
    var question = $(this).find("span");

    if (question.hasClass("faq-close")) {
      question.removeClass("faq-close").addClass("faq-open");
      answers.toggle();
    } else {
      question.removeClass("faq-open").addClass("faq-close");
      answers.toggle();
    }
  });




});  //END DOCUMENT READY
